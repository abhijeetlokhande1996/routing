import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";
import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { M1Component } from './m1/m1.component';

const rArr: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  {

    path: 'main', component: MainComponent, children: [
      { path: 'm1', component: M1Component },
      {
        path: '', redirectTo: 'm1', pathMatch: 'full'
      }, {
        path: '**', component: ErrorPageComponent
      }
    ]
  },
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)


  },
  { path: '**', component: ErrorPageComponent }
]


@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    ErrorPageComponent,
    M1Component,

  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(rArr),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
