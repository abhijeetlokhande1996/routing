import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';

const rArr: Routes = [
  {
    path: 'main', component: MainComponent, children: [
      {
        path: 'login', component: LoginComponent
      },
      {
        path: '', redirectTo: 'login', pathMatch: 'full'
      }
    ]
  }, {
    path: '', redirectTo: 'main', pathMatch: 'full'
  }
]

@NgModule({
  declarations: [LoginComponent, MainComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(rArr)
  ], exports: [
    LoginComponent
  ]
})
export class AuthModule { }
